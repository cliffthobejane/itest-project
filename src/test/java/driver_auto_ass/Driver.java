package driver_auto_ass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;


public class Driver {


    public static WebDriver driver;

    private static DesiredCapabilities capabilities;

    public static WebDriver getDriver(String browserName){




         if(browserName.trim().equalsIgnoreCase("chrome")){

            ChromeOptions options=new ChromeOptions();

            System.setProperty("webdriver.chrome.driver","chromedriver.exe");

            driver=new ChromeDriver(options);

         }
         else

         if(browserName.trim().equalsIgnoreCase("ie")){


             InternetExplorerOptions internetExplorerOptions=new InternetExplorerOptions();


              internetExplorerOptions.ignoreZoomSettings();

             driver=new InternetExplorerDriver(internetExplorerOptions);
         }

         return driver;
    }

}
