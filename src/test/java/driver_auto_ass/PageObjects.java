package driver_auto_ass;

import com.sun.org.apache.regexp.internal.RE;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class PageObjects {



   public static JavascriptExecutor scroll= (JavascriptExecutor)Driver.driver;

   public static Actions actions=new Actions(Driver.driver);

    public static WebElement select(String select){

       return Driver.driver.findElement(By.xpath("//a[@href='https://www.ilabquality.com/"+select.trim().toLowerCase()+"/']"));
    }

    public static WebElement selectFirstAvailableJob(){

        return Driver.driver.findElement(By.cssSelector("a.wpjb-job_title.wpjb-title"));
    }

    public static void scrollDown(WebElement webElement){

        scroll.executeScript("arguments[0].scrollIntoView(true);",webElement);
    }

    public static WebElement selectSA(){
        return Driver.driver.findElement(By.xpath("//a[@href='/careers/south-africa/']"));
    }

    public static WebElement applyButton(){

        return Driver.driver.findElement(By.xpath("//a[@data-wpjb-form='wpjb-form-job-apply']"));
    }

    public static WebElement populateName(){

        return Driver.driver.findElement(By.id("applicant_name"));
    }
    public static WebElement populatePhoneNumber(){

        return Driver.driver.findElement(By.id("phone"));
    }
    public static WebElement populateEmail(){

        return Driver.driver.findElement(By.id("email"));
    }
    public static WebElement sendApplication(){

        return Driver.driver.findElement(By.id("wpjb_submit"));
    }
    public static WebElement errorMessage(){

        return Driver.driver.findElement(By.xpath("//div[@class='wpjb-field']//ul//li"));
    }

}
