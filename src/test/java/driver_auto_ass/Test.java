package driver_auto_ass;

import com.aventstack.extentreports.Status;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Random;

import static jdk.nashorn.internal.objects.NativeMath.random;

public class Test {



    @Before
    public void startBrowser(){


        Reports.extendReports();


        Driver.getDriver(properties().getProperty("browserName"));

        Driver.driver.manage().window().maximize();

    }


    @org.junit.Test
    public void runTest(){

        //use csv file for iterating through test cases
        for (int i=1;i<CSVFileReader.listOfValues().size();i++) {

            CSVFileReader.csvReader("Name",i);
            //open iLab url

            Driver.driver.get(properties().getProperty("url"));

            Reports.extentTest.log(Status.PASS,"Successfully Opened iLab URL");

            threadSleeper(1);

            //select careers

            PageObjects.select("CAREERS").click();

            Reports.extentTest.log(Status.PASS,"Selected Careers successfully");

            threadSleeper(1);

            //Scroll Down to SA Link

            PageObjects.scrollDown(PageObjects.selectSA());

            threadSleeper(1);

            //Select SA
            PageObjects.selectSA().click();

            threadSleeper(1);

            //select first Job
            if(properties().getProperty("browserName").equalsIgnoreCase("ie")){
                PageObjects.scrollDown(PageObjects.selectFirstAvailableJob()) ;
            }
            PageObjects.selectFirstAvailableJob().click();

            threadSleeper(1);

            //Scroll down to Apply button
            if(!properties().getProperty("browserName").equalsIgnoreCase("ie"))
            PageObjects.scrollDown(PageObjects.applyButton());

            threadSleeper(1);

            //Select Apply button
            PageObjects.applyButton().click();

            threadSleeper(1);

            //populate Name
            PageObjects.populateName().sendKeys(CSVFileReader.csvReader("Name",i));

            threadSleeper(1);

            //populate Phone
            PageObjects.populatePhoneNumber().sendKeys(generateCellphoneNumbers());

            threadSleeper(1);

            //populate Email
            PageObjects.populateEmail().sendKeys(CSVFileReader.csvReader("Email",i));

            threadSleeper(1);

            //select submit Application
            PageObjects.sendApplication().click();

            threadSleeper(1);

            Assert.assertEquals("You need to upload at least one file.", PageObjects.errorMessage().getText());

            Reports.extentTest.log(Status.PASS,"You need to upload at least one file. message successfully displayed");


            Reports.extentReports.flush();

        }


    }

    public static StringBuilder generateCellphoneNumbers(){

        Random random=new Random();



        StringBuilder cellNum=new StringBuilder();

        cellNum.append("0");

        for(int i=0;i<9;i++){

            switch (i){
                case 1:
                    cellNum=cellNum.append(random.nextInt(9)).append(" ");
                    break;
                case 4:
                    cellNum=cellNum.append(random.nextInt(9)).append(" ");
                    break;
                default:
                    cellNum=cellNum.append(random.nextInt(9));
            }

        }

        return cellNum;
    }

    @After
    public void closePage(){

        Driver.driver.quit();
    }

    public Properties properties(){


        InputStream   inputStream;
        Properties properties=null;

        try {
          inputStream=  getClass().getClassLoader().getResourceAsStream("ilab-files/property.properties") ;


        properties=new Properties();

        properties.load(inputStream);



        }catch (IOException e){

        }


        return properties;
    }


    private void threadSleeper(double seconds){

        try {

            Thread.sleep((int)seconds*1000);
        }catch (InterruptedException e){

        }
    }




}
