package driver_auto_ass;



import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import java.io.File;

public class Reports {


    public static ExtentReports extentReports;
    public static ExtentHtmlReporter extentHtmlReporter;
    public static ExtentTest extentTest;

    public static void extendReports(){

        extentHtmlReporter=new ExtentHtmlReporter(System.getProperty("user.dir")+"/reports/Ilabassess.html");

        extentReports=new ExtentReports();

        extentTest=extentReports.createTest("IlabAssessment");

        extentReports.attachReporter(extentHtmlReporter);






    }
}
