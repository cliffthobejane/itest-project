package services;


import com.aventstack.extentreports.Status;
import driver_auto_ass.Reports;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.*;


public class ILABAssessmentServices {


    @Test
    public void restServices(){

        Reports.extendReports();

        String requestBody="{\n" +
                "  \"id\": 88,\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"Smoggie\"\n" +
                "  },\n" +
                "  \"name\": \"doggie\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"string\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";





        //verify that a successful message is returned.

        given().when().get("https://dog.ceo/api/breeds/image/random").then().assertThat().statusCode(200);
        Assert.assertTrue(true);
        Reports.extentTest.log(Status.PASS,"Success Message Returned");



        //verify that bulldog is on the list

        while (true){

            if(given().when().get("https://dog.ceo/api/breeds/image/random").getBody().jsonPath().get("message").toString().contains("bulldog")){

                Assert.assertTrue(true);
                Reports.extentTest.log(Status.PASS,"Bulldog Available in the list");
                break;
            }
            }



        //Retrieve name Doggie

        try {

            given().when().get("https://petstore.swagger.io/v2/pet/12").getBody().jsonPath().get("name").toString().contains("doggie");
            Assert.assertEquals(given().when().get("https://petstore.swagger.io/v2/pet/12").getBody().jsonPath().get("name").toString(),"doggie");
            Reports.extentTest.log(Status.PASS,"Doggie is  Returned");
        }catch (Exception e){

            Reports.extentTest.log(Status.FAIL,"Doggie is not Returned");

        }



        //Add a new Pet
        given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .extract().response().then().assertThat().statusCode(200);


        Reports.extentTest.log(Status.PASS,"Success Added a new Pet");

      //  Retrieve the above Pet
       Assert.assertEquals( given().when().get("https://petstore.swagger.io/v2/pet/88").getBody().jsonPath().get("category.name").toString(),"Smoggie");
        Reports.extentTest.log(Status.PASS,"Successfully retrieved new Pet added");


        Reports.extentReports.flush();

        }


    }


